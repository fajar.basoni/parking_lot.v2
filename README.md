# Parking Lot 2.0 Console Application
### Functions of the program: ###

In these programs can be running with the following commands:

create_parking_lot
> to run this command must include parameters. where the system will make parking slots according to the number of these parameters. example "create_parking_lot 9". 9 is a parameter.

park
>to run this command must include the registration number parameter. then these parameters will be registered in the parking system. with the command ["park"] means the system will find an empty spot, then place it there. if a place is available it will provide information of the vehicle placed in the parking slot listed. if not, the system will issue information that the parking lot is full. example "park KA-01-HH-1234" KA-01-HH-1234 is parameter.

leave
>to run this command must include the registration number parameter, and total parking hours. where later the system will search for slot numbers based on the registration_number parameter. Then the system will empty the place and change the information that the place is available. while the hours parameter will calculate the total parking fee that must be paid. example "leave KA-01-HH-3141 4" KA-01-HH-3141 is registration_number and 4 is the parking time / hours.

status
>to execute this command simply by writing the "status" command, then the system will provide parking slot information that has been used.

exit
>this command to stop the program.

by running other than the above command the program will return the response "Invalid Command"

##### < Created by Fajar Basoni > ##### 
