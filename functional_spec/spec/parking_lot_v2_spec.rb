require 'spec_helper'

RSpec.describe 'Parking Lot', type: :aruba do
  let(:command) { run "parking_lot" }
  before(:each) { command.write("create_parking_lot 6\n") }

  it "can create a parking lot" do
    stop_all_commands
    expect(command.output).to end_with("Created a parking lot with 6 slots\n")
  end

  it "can park a car" do
    command.write("park KA-01-HH-1234\n")
    stop_all_commands
    expect(command.output).to end_with("Allocated slot number: 1\n")
  end

  it "can unpark a car" do
    command.write("park KA-01-HH-3141\n")
    command.write("leave KA-01-HH-3141 4\n")
    stop_all_commands
    expect(command.output).to end_with("Registration number KA-01-HH-3141 with slot number 2 is free with Charge 30\n")
  end

  it "can report status" do
    command.write("park KA-01-HH-9999\n")
    command.write("park KA-01-BB-0001\n")
    command.write("park KA-01-HH-7777\n")
    command.write("status\n")
    stop_all_commands
    expect(command.output).to end_with(<<-EOTXT
Slot No.	Registration No
1			KA-01-HH-1234
2			KA-01-HH-9999
3			KA-01-BB-0001
4			KA-01-HH-7777
EOTXT
)
  end

  it "can unpark a car" do
    command.write("park KA-01-HH-2700\n")
    command.write("park KA-01-HH-0078\n")
    command.write("leave KA-01-HH-2700 4\n")
    command.write("leave KA-01-HH-0078 6\n")
    stop_all_commands
    expect(command.output).to end_with("Registration number KA-01-HH-2700 with slot number 5 is free with Charge 30\nRegistration number KA-01-HH-0078 with slot number 6 is free with Charge 30\n")
  end
end