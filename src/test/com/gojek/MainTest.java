package com.gojek;

import com.gojek.Main;
import java.util.Arrays;
import static org.junit.Assert.*;

public class MainTest  {
    Main objMain = new Main();

    @org.junit.jupiter.api.Test
    void action() {
        assertEquals(Arrays.asList("create_parking_lot", "6"), objMain.Action("create_parking_lot 6"));
        assertEquals(Arrays.asList("park", "KA-01-HH-1234"), objMain.Action("park KA-01-HH-1234"));
        assertEquals(Arrays.asList("leave", "KA-01-HH-3141", "4"), objMain.Action("leave KA-01-HH-3141 4"));
        assertEquals(Arrays.asList("status"), objMain.Action("status"));
    }
}