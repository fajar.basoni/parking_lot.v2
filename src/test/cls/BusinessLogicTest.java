package cls;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BusinessLogicTest {
    BusinessLogic objBusinessLogic = new BusinessLogic();
    int park_lot = 6;

    @Test
    public void createParkingLot() throws Exception{
        assertDoesNotThrow(() -> objBusinessLogic.CreateParkingLot(park_lot));
    }

    @Test
    void park() throws Exception {
        objBusinessLogic.CreateParkingLot(6);

        assertDoesNotThrow(() -> objBusinessLogic.Park("KA-01-HH-1234"));
        assertDoesNotThrow(() -> objBusinessLogic.Park("KA-01-HH-9999"));
        assertDoesNotThrow(() -> objBusinessLogic.Park("KA-01-BB-0001"));
        assertDoesNotThrow(() -> objBusinessLogic.Park("KA-01-HH-7777"));
        assertDoesNotThrow(() -> objBusinessLogic.Park("KA-01-HH-2701"));
        assertDoesNotThrow(() -> objBusinessLogic.Park("KA-01-HH-3141"));
    }

    @Test
    void leave() throws Exception {
        assertDoesNotThrow(() -> objBusinessLogic.Leave("KA-01-HH-3141", "4"));
    }

    @Test
    void status() throws Exception {
        objBusinessLogic.CreateParkingLot(2);
        objBusinessLogic.Park("KA-01-HH-1234");
        objBusinessLogic.Park("KA-01-HH-5678");

        assertDoesNotThrow(() -> objBusinessLogic.Status());
    }

    @Test
    void writeConsoleAndOutputFile() throws Exception {
        assertDoesNotThrow(() -> objBusinessLogic.WriteConsoleAndOutputFile("any text output"));
    }

    @Test
    void clearOutputFile() throws Exception {
        assertDoesNotThrow(() -> objBusinessLogic.ClearOutputFile());
    }

    @Test
    void GetSlotNumberByRegisterationNumber() throws Exception{
        objBusinessLogic.CreateParkingLot(2);
        objBusinessLogic.Park("KA-01-HH-1234");
        objBusinessLogic.Park("KA-01-HH-5678");

        assertEquals(objBusinessLogic.GetSlotNumberByRegisterationNumber("KA-01-HH-1234"), "1");
        assertEquals(objBusinessLogic.GetSlotNumberByRegisterationNumber("KA-01-HH-5678"), "2");
        assertEquals(objBusinessLogic.GetSlotNumberByRegisterationNumber("KA-01-HH-5679"), "");
    }

    @Test
    void GetCharge() throws Exception{
        assertEquals(objBusinessLogic.GetCharge(0), "10");
        assertEquals(objBusinessLogic.GetCharge(1), "10");
        assertEquals(objBusinessLogic.GetCharge(2), "10");
        assertEquals(objBusinessLogic.GetCharge(3), "20");
        assertEquals(objBusinessLogic.GetCharge(4), "30");
        assertEquals(objBusinessLogic.GetCharge(5), "40");
        assertEquals(objBusinessLogic.GetCharge(6), "50");
    }
}