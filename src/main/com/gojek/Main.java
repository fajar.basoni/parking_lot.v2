package com.gojek;

import cls.BusinessLogic;
import main.cls.Constanta;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
        BusinessLogic.ClearOutputFile();
        try {
            BufferedReader reader = reader = new BufferedReader(new InputStreamReader(System.in));

            String input = reader.readLine();
            while (!input.equals("")){
                List<String> commandName = Action(input.trim().toLowerCase());
                switch (commandName.get(0)){
                    case Constanta.ACTION_CREATE:
                        BusinessLogic.CreateParkingLot(Integer.parseInt(commandName.get(1)));
                        break;
                    case Constanta.ACTION_PARK:
                        BusinessLogic.Park(commandName.get(1));
                        break;
                    case Constanta.ACTION_LEAVE:
                        BusinessLogic.Leave(commandName.get(1), commandName.get(2));
                        break;
                    case Constanta.ACTION_STATUS:
                        BusinessLogic.Status();
                        break;
                    case Constanta.ACTION_EXIT:
                        System.exit(0);
                        break;
                    default:
                        BusinessLogic.WriteConsoleAndOutputFile(Constanta.MESSAGE_INVALID_ACTION);
                }
                input = reader.readLine();
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    //Method for generate action name
    public static List<String> Action(String CommandName){
        List<String> items = Arrays.asList(CommandName.split("\\s+"));
        return items;
    }
}

