package main.cls;

import java.nio.file.Paths;

public class Constanta {
    public static final String ACTION_CREATE = "create_parking_lot";
    public static final String ACTION_PARK = "park";
    public static final String ACTION_LEAVE = "leave";
    public static final String ACTION_STATUS = "status";
    public static final String ACTION_EXIT = "exit";

    public static final int CHARGE_PER_HOUR = 10;

    public static final String OUTPUT_FILE = System.getProperty("user.dir") + Paths.get("/functional_spec/fixtures/file_output.txt");
    public static final String INPUT_FILE = System.getProperty("user.dir") + Paths.get("/functional_spec/fixtures/file_input.txt");

    public static final String MESSAGE_CREATE = "Created a parking lot with %s slots";
    public static final String MESSAGE_PARK_SUCCESS = "Allocated slot number: %s";
    public static final String MESSAGE_PARK_FULL = "Sorry, parking lot is full";
    public static final String MESSAGE_LEAVE = "Registration number %s with slot number %s is free with Charge %s";
    public static final String MESSAGE_STATUS = "Slot No." + "\t" + "Registration No";
    public static final String MESSAGE_NOT_FOUND = "Registration number %s not found";
    public static final String MESSAGE_INVALID_ACTION = "Invalid Command";
}
