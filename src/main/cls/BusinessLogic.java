package cls;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import main.cls.Model;
import main.cls.Constanta;

public class BusinessLogic {
    //Method for generate parking lots available
    public static void CreateParkingLot(int param)
    {
        //clear first parking lot
        Model.elements.clear();
        for(int i = 1; i <= param; i++)
        {
            Model.Properties element = new Model.Properties();
            element.setSlot_number(String.valueOf(i));
            element.setRegistration_number("");

            Model.elements.add(element);
        }
        Model.setElements(Model.elements);
        WriteConsoleAndOutputFile(String.format(Constanta.MESSAGE_CREATE, Model.getElements().size()));
    }

    //Method for set parking lot cars
    public static void Park(String registration_number){
        boolean is_full = true;
        for (Model.Properties item : Model.getElements()) {
            if(item.getRegistration_number() == ""){
                item.setRegistration_number(registration_number);

                Model.elements.set(Integer.parseInt(item.getSlot_number()) - 1, item);
                Model.setElements(Model.elements);
                is_full = false;
                WriteConsoleAndOutputFile(String.format(Constanta.MESSAGE_PARK_SUCCESS, item.getSlot_number()));
                break;
            }
        }
        if(is_full){
            WriteConsoleAndOutputFile(Constanta.MESSAGE_PARK_FULL);
        }
    }

    //Method for set leave cars from parking lots
    public static void Leave(String registration_number, String hours){
        String slot_number = GetSlotNumberByRegisterationNumber(registration_number);
        if(!slot_number.isEmpty()){
            for (Model.Properties item: Model.getElements()) {
                if(Integer.parseInt(item.getSlot_number()) == Integer.parseInt(slot_number)){
                    item.setRegistration_number("");

                    Model.elements.set(Integer.parseInt(item.getSlot_number()) - 1, item);
                    Model.setElements(Model.elements);
                    WriteConsoleAndOutputFile(String.format(Constanta.MESSAGE_LEAVE, registration_number, item.getSlot_number(), GetCharge(Integer.parseInt(hours))));
                    break;
                }
            }
        }
        else {
            WriteConsoleAndOutputFile(String.format(Constanta.MESSAGE_NOT_FOUND, registration_number));
        }
    }

    //Method for view status parking lots
    public static void Status(){
        WriteConsoleAndOutputFile(Constanta.MESSAGE_STATUS);
        for (Model.Properties item : Model.getElements()){
            if(item.getRegistration_number() != "") {
                WriteConsoleAndOutputFile(item.getSlot_number() + "\t\t\t" + item.getRegistration_number());
            }
        }
    }

    //Method for search slot number by registration number
    public static String GetSlotNumberByRegisterationNumber(String param){
        String output = "";
        for (Model.Properties item : Model.getElements()){
            if(String.valueOf(item.getRegistration_number()).toLowerCase().equals(param.toLowerCase())) {
                output = item.getSlot_number();
            }
        }
        return output;
    }

    public static String GetCharge(int hours){
        int charge = 0;
        if(hours <= 2){
            charge = Constanta.CHARGE_PER_HOUR;
        }
        else{
            charge = (hours * Constanta.CHARGE_PER_HOUR) - Constanta.CHARGE_PER_HOUR;
        }
        return String.valueOf(charge);
    }

    //Method for write output file
    public static void WriteConsoleAndOutputFile(String text){
        try{
            System.out.println(text);
            Files.write(Paths.get(Constanta.OUTPUT_FILE),
                    String.valueOf(text + "\n" ).getBytes(),
                    StandardOpenOption.APPEND);
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    //on the first run system will clear output file
    public static void ClearOutputFile(){
        try{
            PrintWriter writer = new PrintWriter(Constanta.OUTPUT_FILE);
            writer.print("");
            writer.close();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
