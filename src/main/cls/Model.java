package main.cls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Model {
    public static class Properties{
        private String slot_number;
        private String registration_number;

        public String getSlot_number() {
            return slot_number;
        }

        public String getRegistration_number() {
            return registration_number;
        }

        public void setSlot_number(String slot_number) {
            this.slot_number = slot_number;
        }

        public void setRegistration_number(String registration_number) {
            this.registration_number = registration_number;
        }
    }

    public static List<Properties> elements = new ArrayList<Properties>();

    public static List<Properties> getElements() {
        return elements;
    }

    public static void setElements(List<Properties> elements) {
        Model.elements = elements;
    }
}
